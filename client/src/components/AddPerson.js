import React from 'react'

const AddPerson = props => (
  <div className="input-group">
    <input
      type="text"
      className="form-control"
      placeholder="Add people"
      onChange={props.inputChange}
      value={props.value}
    />
    <button className="btn btn-secondary" onClick={props.savePerson}>+</button>
  </div>
)
export default AddPerson
