import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import DrinkButton from './DrinkButton';
import { addDrink, getSummary, finalizeSession } from '../reducers/session'
import c from '../constants'

const { BEER, SHOT, WINE } = c.DRINK_TYPE

class List extends Component {
  render() {
    const empty = <span>Add people here</span>
    const { sessionId } = this.props.session

    const list = (
      <ul className="list-group">
        {this.props.persons.map((p, i) => (
          <li
            className="list-group-item d-flex justify-content-between align-items-center"
            key={i}
          >
            {p.name}
            {/* TODO: Refactor this section */}
            <div
              className=""
            >
              <DrinkButton
                number={p.drinks.filter(d => d.type === 'BEER').length}
                drinkType={BEER}
                action={() => this.props.addDrink(p.name, BEER)}
              />
              <DrinkButton
                number={p.drinks.filter(d => d.type === 'WINE').length}
                drinkType={WINE}
                action={() => this.props.addDrink(p.name, WINE)}
              />
              <DrinkButton
                number={p.drinks.filter(d => d.type === 'SHOT').length}
                drinkType={SHOT}
                action={() => this.props.addDrink(p.name, SHOT)}
              />
            </div>
          </li>
        ))}
        <li style={{ listStyle: 'none' }}>
          {/* <button
            className="btn btn-secondary"
            onClick={() => this.props.getSummary({ sessionId })}
          >
            Summary
          </button> */}

          <button
            className="btn btn-secondary"
            onClick={() => this.props.finalizeSession({ sessionId })}
          >
            Finalize
          </button>
        </li>
      </ul>
    )

    return (
      <div>
        {this.props.persons.length === 0 ? empty : list}
      </div>
    )
  }
}

const mapStateToProps = ({ session }) => ({
  persons: session.persons,
  session,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  addDrink,
  getSummary,
  finalizeSession,
}, dispatch)

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(List))