import React from 'react'

const CreateSession = props => (
  <div className="input-group">
    <input
      type="text"
      className="form-control"
      placeholder="Session name"
      onChange={props.inputChange}
      value={props.value}
    />
    <button onClick={props.createSession} className="btn btn-secondary">Create Session</button>
  </div>
)

export default CreateSession