import React from 'react'
import { Link } from 'react-router-dom';

const Header = props => {
  if (props.isAuth) {
    return (
      <div className="container" style={{ marginBottom: '2rem' }}>
        <header className="d-flex align-items-center">
          <Link to="/" className="btn btn-text" style={{ marginRight: 'auto', fontSize: '1.25rem' }}>KnowYourPenaky</Link>
          <button className="btn btn-danger m-l-auto" onClick={props.logout}>Logout</button>
        </header>
      </div>
    )
  } else {
    return (
      <div className="container" style={{ marginBottom: '2rem' }}>
        <header style={{ display: 'flex', alignItems: 'flex-end' }}>
          <Link to="/" className="btn btn-text" style={{ marginRight: 'auto', fontSize: '1.25rem' }}>KnowYourPenaky</Link>
          {/* <Link to="/login" className="btn btn-link">Login</Link> */}
          <Link to="/register" className="btn btn-warning">Register</Link>
        </header>
      </div>
    )
  }
}


export default Header