import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBeer, faWineGlass, faGlassMartini, faQuestion } from '@fortawesome/free-solid-svg-icons'
import c from '../constants'

const selectIcon = drinkType => {
  switch (drinkType) {
    case c.DRINK_TYPE.BEER:
      return faBeer
    case c.DRINK_TYPE.WINE:
      return faWineGlass
    case c.DRINK_TYPE.SHOT:
      return faGlassMartini
    default:
      return faQuestion
  }
}

const DrinkButton = props => (
  <button className="btn" onClick={props.action}>
    {`${props.number} ` || `0 `}
    <FontAwesomeIcon icon={selectIcon(props.drinkType)} />
  </button>
)

export default DrinkButton