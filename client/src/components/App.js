import React from 'react'
import { Route, Link, Redirect, Switch, withRouter } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../bootstrap4-neon-glow.css'
import Home from './Home'
import Login from './Login'
import Header from './Header'
import Register from './Register'
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';
import { connect } from 'react-redux'
import { logout } from '../reducers/auth'

const ProtectedRoute = ({ component: Component, isAuth, ...props }) => (
  <Route {...props} render={(props) => (
    isAuth
      ? <Component {...props} />
      : <Redirect to="/login" />
  )} />
)


class App extends React.Component {
  componentWillMount() {

  }
  render() {
    return (
      <div>
        <Header isAuth={this.props.isAuth} logout={this.props.logout} />
        <main>
          <ProtectedRoute exact path="/" isAuth={this.props.isAuth} component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
        </main>
        <div className="container" style={{ position: 'absolute', bottom: 0, display: 'flex', justifyContent: 'flex-end', left: '50%', transform: 'translate(-50%, 0)' }}>
          <span className="text-success">v0.1-alpha</span>
        </div>
      </div >
    )
  }
}


const mapStateToProps = ({ status, auth }) => ({
  isAuth: auth.auth,
  isLoading: status.isLoading,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  redirectToHome: () => push('/register'),
  logout,
}, dispatch)

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App))