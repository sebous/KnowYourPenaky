import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { login, fbLogin } from '../reducers/auth'
import { Form, FormGroup, FormControl, FormLabel, Button } from 'react-bootstrap';
import FBloginBtn from 'react-facebook-login';

class Login extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      formValid: false,
    }
  }

  fbAuth = response => {
    console.log(response)
    this.props.fbLogin(response)
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value })

  validate = () => {
    if (Object.keys(this.state).some(key => this.state[key] === '')) {
      this.setState({ formValid: false })
      return false
    }
    return true
  }

  submit = e => {
    e.preventDefault()
    if (this.validate()) {
      const { email, password } = this.state
      this.props.login(email, password)
    } else {

    }
  }

  render() {
    return (
      <div className="container">
        <Form onSubmit={this.submit} validated={this.state.formValid}>
          <FormGroup>
            <FormLabel>Login</FormLabel>
            <FormControl
              type="email"
              required
              placeholder="Your email"
              name="email"
              onChange={this.handleChange}
              value={this.state.login}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel>Password</FormLabel>
            <FormControl
              type="password"
              required
              name="password"
              onChange={this.handleChange}
              value={this.state.password}
            />
          </FormGroup>
          <FormGroup style={{ marginTop: '2rem' }}>
            <Button variant="primary" size="lg" type="submit" style={{ marginRight: '1.5rem' }}>Log in</Button>
            <FBloginBtn
              appId={process.env.REACT_APP_FACEBOOK_APP_ID}
              autoLoad={false}
              fields="name,picture"
              scope="public_profile"
              callback={this.fbAuth}
              cssClass="btn btn-secondary btn-lg"
              icon="fa-facebook"
              textButton=""
            />
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  login,
  fbLogin,
}, dispatch)


export default connect(null, mapDispatchToProps)(Login)
