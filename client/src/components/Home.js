import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AddPerson from './AddPerson';
import CreateSession from './CreateSession';
import { createSession, addPerson } from '../reducers/session'
import List from './List';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addPersonInputVal: '',
      createSessionInputVal: '',
    }
  }

  handleCreateSessionChange = e => this.setState({ createSessionInputVal: e.target.value })

  handleAddPersonChange = e => this.setState({ addPersonInputVal: e.target.value })

  createSession = () => {
    if (this.state.createSessionInputVal !== '') {
      this.props.createSession(this.state.createSessionInputVal, this.props.user.id)
      this.setState({ createSessionInputVal: '' })
    } else {
      alert('fock sake')
    }
  }

  savePerson = () => {
    if (this.state.addPersonInputVal !== '') {
      this.props.addPerson(this.state.addPersonInputVal, this.props.session.sessionId)
      this.setState({ addPersonInputVal: '' })
    } else {
      alert('giff name boi')
    }
  }

  render() {
    return (
      <div className="container">
        <h3>{this.props.session.sessionName}</h3>
        {!this.props.sessionActive ? (
          <CreateSession
            inputChange={this.handleCreateSessionChange}
            createSession={this.createSession}
            value={this.state.createSessionInputVal}
          />
        ) : (
            <div>
              <AddPerson
                inputChange={this.handleAddPersonChange}
                savePerson={this.savePerson}
                value={this.state.addPersonInputVal}
              />
              <List />
            </div>
          )}
      </div>
    )
  }

}
const mapStateToProps = ({ session, auth }) => ({
  sessionActive: !!session.sessionId && !session.finalized,
  session,
  user: auth.user,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  changePage: () => push('/about-us'),
  createSession,
  addPerson,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)