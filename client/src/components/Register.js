import React, { Component } from 'react'
import { Form, FormGroup, FormLabel, FormControl, Button } from 'react-bootstrap'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { register } from '../reducers/auth'

class Register extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      name: '',
      password: '',
      formValid: false,
    }
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value })

  validate = () => {
    if (Object.keys(this.state).some(key => this.state[key] === '')) {
      this.setState({ formValid: false })
      return false
    }
    return true
  }

  submit = e => {
    e.preventDefault()
    if (this.validate()) {
      const { email, name, password } = this.state
      this.props.register({ email, name, password })
    } else {
      alert('not valid')
    }
  }

  render() {
    return (
      <div className="container">
        <Form onSubmit={this.submit} validated={this.state.formValid}>
          <FormGroup>
            <FormLabel>Email</FormLabel>
            <FormControl
              type="email"
              required
              placeholder="Your email"
              name="email"
              onChange={this.handleChange}
              value={this.state.email}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel>Name</FormLabel>
            <FormControl
              type="text"
              required
              placeholder="Full name"
              name="name"
              onChange={this.handleChange}
              value={this.state.name}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel>Password</FormLabel>
            <FormControl
              type="password"
              required
              name="password"
              onChange={this.handleChange}
              value={this.state.password}
            />
          </FormGroup>
          <Button variant="primary" type="submit" size="lg">Sign up</Button>
        </Form>
      </div>
    )
  }
}

const mapStateToProps = ({ status }) => ({

})

const mapDispatchToProps = dispatch => bindActionCreators({
  register,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Register)
