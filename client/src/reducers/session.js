import request from "../helpers/request";
import errorHandler from '../helpers/errorHandler'
import drinkType from "../constants/drinkType";


export const CREATE_SESSION = 'CREATE_SESSION'
export const FINALIZE_SESSION = 'CLOSE_SESSION'
export const ADD_PERSON = 'ADD_PERSON'
// export const REMOVE_PERSON = 'REMOVE_PERSON'
export const ADD_DRINK = 'ADD_DRINK'
export const SUMMARIZE_SESSION = 'SUMMARIZE_SESSION'
export const LOAD_SESSION = 'LOAD_SESSION'

const initialState = {
  sessionId: null,
  sessionName: '',
  persons: [],
  summary: [],
  finalized: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SESSION:
      return {
        ...state,
        sessionId: action.data._id,
        sessionName: action.data.name,
      }
    case LOAD_SESSION:
      return {
        ...state,
        sessionId: action.data._id,
        sessionName: action.data.name,
        persons: [...action.data.people],
      }
    case ADD_PERSON:
      return {
        ...state,
        persons: [...action.data.people],
      }
    case ADD_DRINK:
      return {
        ...state,
        persons: [...action.data.people],
      }
    case SUMMARIZE_SESSION:
      return {
        ...state,
        summary: [...action.data],
      }
    case FINALIZE_SESSION:
      return initialState
    default:
      return state
  }
}

export const createSession = (name, userId) => {
  return async (dispatch, getState) => {
    try {
      const session = await request.post('/session/create', { name, userId })
      console.log(session)
      const { data } = session
      dispatch({ type: CREATE_SESSION, data })
    } catch (err) {
      alert(errorHandler(err))
    }
  }
}

export const addPerson = (name, sessionId) => {
  return async (dispatch) => {
    try {
      const updatedSession = await request.post('/session/addPerson', { name, sessionId })
      const { data } = updatedSession
      dispatch({ type: ADD_PERSON, data })
    } catch (err) {
      alert(errorHandler(err))
    }
  }
}

export const addDrink = (personName, drinkType) => {
  return async (dispatch, getState) => {
    try {
      const state = getState()
      // console.log(state)
      const session = await request.post('/session/addDrink', {
        name: personName,
        sessionId: state.session.sessionId,
        drinkType,
      })
      const { data } = session
      dispatch({ type: ADD_DRINK, data })
    }
    catch (err) {
      console.log(err)
    }
  }
}

export const getSummary = ({ sessionId }) => async (dispatch) => {
  try {
    const summary = await request.post('/session/summary', { sessionId })
    const { data } = summary
    dispatch({ type: SUMMARIZE_SESSION, data })
  } catch (err) {
    console.log(err)
  }
}

export const finalizeSession = ({ sessionId }) => async dispatch => {
  try {
    await request.post('/session/finalize', { sessionId })
    dispatch({ type: FINALIZE_SESSION })
  } catch (err) {
    console.log(err)
  }
}