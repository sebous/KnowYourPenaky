import moment from 'moment'
import { IS_LOADING, LOADED } from './status'
import request from '../helpers/request'
import errorHandler from '../helpers/errorHandler'
import { history } from '../store'
import { LOAD_SESSION } from './session'

export const REGISTERED = 'REGISTERED'
export const LOGGED_IN = 'LOGGED_IN'
export const LOGGED_OUT = 'LOGGED_OUT'
export const REFRESH_TOKEN = 'REFRESH_TOKEN'


const initialState = {
  auth: false,
  tokenAquired: false,
  tokenDate: null,
  token: null,
  user: {},
}

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTERED:
      return {
        ...state,
        auth: action.data.auth,
        tokenAquired: true,
        tokenDate: moment().toDate(),
        user: action.data.user,
      }

    case LOGGED_IN:
      return {
        ...state,
        auth: action.data.auth,
        tokenAquired: true,
        tokenDate: moment().toDate(),
        token: action.data.token,
        user: action.data.user,
      }
    case LOGGED_OUT:
      return {
        ...state,
        auth: false,
        tokenAquired: false,
        tokenDate: null,
        user: null,
      }
    default:
      return state
  }
}

export const register = (loginDetails) => {
  return async (dispatch) => {
    try {
      dispatch({ type: IS_LOADING })
      const registration = await request.post('/user/register', loginDetails)
      if (registration.status === 201) {
        const { data } = registration
        request.token(data)
        dispatch({ type: REGISTERED, data })
        dispatch({ type: LOADED })
        history.push('/')
      }
    } catch (err) {
      alert(errorHandler(err))
      dispatch({ type: LOADED })
    }
  }
}

export const login = (email, password) => {
  return async (dispatch) => {
    try {
      dispatch({ type: IS_LOADING })
      const login = await request.post('/user/login', { email, password })
      if (login.status !== 200) throw new Error('login failed')

      const { data } = login
      request.token(data)
      dispatch({ type: LOGGED_IN, data })

      const lastSession = await request.post('/session/last', { userId: data.user.id })

      if (lastSession.data !== '') {
        const { data } = lastSession;
        dispatch({ type: LOAD_SESSION, data })
      }
      dispatch({ type: LOADED })
      history.push('/')
    } catch (err) {
      alert(errorHandler(err))
      dispatch({ type: LOADED })
    }
  }
}

export const fbLogin = ({ accessToken, userID, name }) => async (dispatch) => {
  try {
    dispatch({ type: IS_LOADING })
    const login = await request.post('/user/fb-login', { accessToken, userID, name })
    if (login.status !== 200) throw new Error('login failed')

    const { data } = login
    request.token(data)
    dispatch({ type: LOGGED_IN, data })

    const lastSession = await request.post('/session/last', { userId: data.user.id })

    if (lastSession.data !== '') {
      const { data } = lastSession;
      dispatch({ type: LOAD_SESSION, data })
    }
    dispatch({ type: LOADED })
    history.push('/')
  } catch (err) {
    alert(errorHandler(err))
    dispatch({ type: LOADED })
  }
}

export const logout = () => {
  return (dispatch) => {
    dispatch({ type: LOGGED_OUT })
    history.push('/')
  }
}

export const fakeLogin = () => {
  return async (dispatch) => {
    try {
      const fakeLoginData = {
        auth: true,
        user: {
          email: 'dummyuser@gmail.com',
          name: 'dummy user',
          id: 'abc123',
        }
      }
      dispatch({ type: IS_LOADING })
      dispatch({ type: LOGGED_IN, data: fakeLoginData })
      dispatch({ type: LOADED })
      history.push('/')
    } catch (error) {
      console.log(error)
      dispatch({ type: LOADED })
    }
  }
}

export const refreshToken = () => {
  return async (dispatch) => {
    try {
      const result = await request.get('/user/refresh-token')
      request.token(result.data)
    } catch (error) {
      console.log(error)
    }
  }
}