export const IS_LOADING = 'IS_LOADING'
export const LOADED = 'LOADED'

const initialState = {
  isLoading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case IS_LOADING:
      return {
        ...state,
        isLoading: true,
      }
    case LOADED:
      return {
        ...state,
        isLoading: false,
      }
    default:
      return state
  }
}