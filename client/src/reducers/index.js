import { combineReducers } from 'redux'
import auth from './auth'
import status from './status'
import session from './session'

export default combineReducers({
  auth,
  session,
  status,
})