import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import throttle from 'lodash/throttle'
import store, { history } from './store'
import { saveState } from './helpers/localStorage'
import App from './components/App'
import * as serviceWorker from './serviceWorker';

const target = document.querySelector('#root')

store.subscribe(throttle(() => {
  saveState(store.getState())
}), 300)

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  target
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
