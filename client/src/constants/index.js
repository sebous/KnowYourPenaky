import DRINK_TYPE from './drinkType'

export default Object.freeze({
  API: {
    URL: '/api',
  },
  LOCAL_STORAGE: {
    STORE: 'store',
  },
  DRINK_TYPE,
})