import c from '../constants'
import request from './request'

export const saveState = state => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem(c.LOCAL_STORAGE.STORE, serializedState)
  } catch (err) {
    // notify somewhere
  }
}

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem(c.LOCAL_STORAGE.STORE)
    if (serializedState === null) return undefined
    const stateObj = JSON.parse(serializedState)
    const { token } = stateObj.auth
    request.token({ token })
    return stateObj
  } catch (err) {
    return undefined
  }
}
