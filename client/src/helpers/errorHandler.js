/**
 * 
 * @param {any} err
 * @returns {string} error message
 */
const errorHandler = err => {
  if (!err || !err.response) return 'invalid response'

  const { response } = err
  return response.data.error || response.statusText
}

export default errorHandler
