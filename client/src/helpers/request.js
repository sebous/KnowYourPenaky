import axios from 'axios'
import c from '../constants'

let baseConfig = {
  timeout: 1000,
  headers: {
    'Accept': 'application/json',
    'content-type': 'application/json',
  },
}

const apiUrl = url => c.API.URL + url

export default {
  get: (url) => {
    return axios.get(apiUrl(url), baseConfig)
  },
  post: (url, data) => {
    return axios.post(apiUrl(url), data, baseConfig)
  },
  token: (data) => {
    baseConfig.headers['x-access-token'] = data.token
  },
}