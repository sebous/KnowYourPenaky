module.exports = {
  apps: [{
    name: 'KnowYourPenaky API',
    script: './dist/app.js',
    env: {
      NODE_ENV: 'production',
    },
  }],
}