type Secrets = {
    username: string
    password: string
    link: string
    jwtSecret: string
}

const secrets: Secrets = {
    username: 'seb2',
    password: 'Seba1234',
    link: 'ds046037.mlab.com:46037/knowyourpenaky',
    jwtSecret: '123456ab',
}

export default secrets