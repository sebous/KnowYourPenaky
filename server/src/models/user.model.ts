import { Schema, model, Document } from 'mongoose'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

import secrets from '../config/secrets'
import { tokenExpire } from '../config'

export type UserDocument = Document & {
  email: string
  password: string
  name: string
  facebookId: string
  passwordMatches: (pass: string) => Promise<boolean>
  safeForm: () => UserSafeForm
  generateToken: () => string
}

export type UserSafeForm = {
  email: string
  name: string
  id: string
  facebookId?: string
}

export const userSchema = new Schema(
  {
    email: { type: String, unique: true },
    password: String,
    name: String,
    facebookId: String,
  },
  {
    timestamps: true,
  }
)

userSchema.pre<UserDocument>('save', async function (this: UserDocument, next) {
  try {
    if (!this.isModified('password')) return next()
    const hash = await bcrypt.hash(this.password, 10)
    this.password = hash
    return next()
  } catch (err) {
    next(err)
  }
})

userSchema.method({
  async passwordMatches(this: UserDocument, pass: string) {
    return bcrypt.compare(pass, this.password)
  },
  safeForm() {
    return {
      email: this.email,
      name: this.name,
      id: this.id,
      facebookId: this.facebookId,
    }
  },
  generateToken() {
    return jwt.sign({ sub: this._id }, secrets.jwtSecret, {
      expiresIn: tokenExpire,
    })
  }
})

export const User = model<UserDocument>('User', userSchema)
