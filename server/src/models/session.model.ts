import { Schema, model, Document } from 'mongoose'
import { Moment } from 'moment'

export type Drink = {
  type: DrinkEnum
  timeOrdered: Date
}
export type Person = {
  name: string
  drinks: Drink[]
}

export enum DrinkEnum {
  BEER,
  WINE,
  SHOT,
}

export type SessionDocument = Document & {
  name?: string
  people: Person[]
  finalized: boolean
  datePayed: Date | null
  createdBy: string
}

export const sessionSchema = new Schema(
  {
    name: String,
    people: Array,
    datePayed: { type: Date, default: null },
    finalized: { type: Boolean, default: false },
    createdBy: String,
  },
  {
    timestamps: true,
  }
)

export const Session = model<SessionDocument>('Session', sessionSchema)
