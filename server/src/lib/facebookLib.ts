import axios from 'axios';

// TODO: pass APP_SECRET as env variable
const APP_ID = '766355567152585';
const APP_SECRET = '57a3716a62dc5a847645b0e4cacc78ac';
const CHECK_TOKEN_URL = 'https://graph.facebook.com/debug_token';

export const checkToken = async (token: string) => {
  try {
    const res = await axios.get(CHECK_TOKEN_URL, {
      params: {
        input_token: token,
        access_token: `${APP_ID}|${APP_SECRET}`,
      },
    });
    const { data } = res.data;
    return data.is_valid as boolean;
  } catch (err) {
    return false;
  }
}
