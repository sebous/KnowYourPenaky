import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt'
import { UserDocument, User } from '../models/user.model'
import secrets from '../config/secrets'

const loginStrat = new LocalStrategy(async (email, pass, done) => {
    try {
        const user = await User.findOne({ email })
        if (!user) return done(null, false, { message: 'incorrect username' })
        if (!user.passwordMatches(pass)) return done(null, false, { message: 'incorrect password' })
        return done(null, user)
    } catch (err) {
        return done(err)
    }
})

const jwtStrat = new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromHeader('x-access-token'),
        secretOrKey: secrets.jwtSecret,
    },
    async (payload, done) => {
        try {
            const user = await User.findById(payload.sub)
            return done(null, user || false)
        } catch (err) {
            return done(err, false)
        }
    }
)

passport.use(loginStrat)
passport.use(jwtStrat)

export const requireSignIn = passport.authenticate('local', { session: false })
export const requireAuth = passport.authenticate('jwt', { session: false })