import mongoose from 'mongoose'
import status from 'http-status'
import { Request, Response, NextFunction } from 'express'

import { UserDocument, User } from '../models/user.model'
import { checkToken } from '../lib/facebookLib'

export default class UserController {

  public async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password, name } = req.body
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        email,
        password,
        name,
      })
      const savedUser = await user.save()
      const token = savedUser.generateToken()
      res.status(status.CREATED).send({ auth: true, token, user: savedUser.safeForm() })
    } catch (err) {
      next(err)
    }
  }

  public async login(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body
      const user = await User.findOne({ email })
      if (!user) return res.status(status.UNAUTHORIZED).send({ auth: false, token: null })

      const passwordMatch = await user.passwordMatches(password)
      if (!passwordMatch) return res.status(status.UNAUTHORIZED).send({ auth: false, token: null })

      const token = user.generateToken()
      res.status(status.OK).send({ auth: true, token, user: user.safeForm() })
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send({ auth: false, token: null })
    }
  }

  public async fbLogin(req: Request, res: Response) {
    try {
      const { accessToken, userID, name } = req.body

      const isValid = await checkToken(accessToken)
      if (!isValid) return res.status(status.NOT_FOUND).send({ auth: false, token: null })

      const user = await User.findOne({ facebookId: userID })

      if (!user) {
        const newUser = await User.create({
          facebookId: userID,
          name,
        })
        const token = newUser.generateToken()
        return res.status(status.OK).send({ auth: true, token, user: newUser.safeForm() })
      }

      // user already exists
      const token = user.generateToken();
      return res.status(status.OK).send({ auth: true, token, user: user.safeForm() })

    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send({ auth: false, token: null })
    }
  }

  public async logout(req: Request, res: Response) {
    res.status(status.OK).send({ auth: false, token: null })
  }

  public async testToken(req: Request, res: Response, next: NextFunction) {
    try {
      const user = await User.findById(req.user.id)
      if (!user) return res.status(status.NOT_FOUND).send({ auth: false, token: null })

      res.status(status.OK).send({ auth: true, user: user.safeForm() })
    } catch (err) {
      res.status(status.UNAUTHORIZED).send({ auth: false })
    }
  }

  public async refreshToken(req: Request, res: Response, next: NextFunction) {
    try {
      const user = await User.findById(req.user.id)
      if (!user) return res.status(status.NOT_FOUND).send({ auth: false, token: null })

      res.status(status.OK).send({ auth: true, token: user.generateToken() })
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send({ auth: false, err })
    }
  }

  public async getOne(req: Request, res: Response, next: NextFunction) {
    try {
      const user = await User.findById(req.params.id)
      if (!user) return res.status(status.NOT_FOUND).send({ error: 'User not found' })
      res.status(status.OK).send(user.safeForm())
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const users = await User.find()
      console.log(JSON.stringify(users))
      res.status(status.OK).send(users.map(u => u.safeForm()))
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async deleteOne(req: Request, res: Response, next: NextFunction) {
    try {
      const removedUser = await User.findByIdAndRemove(req.params.id)
      if (!removedUser) return res.status(status.NOT_FOUND).send({ auth: false, token: null })

      res.status(status.OK).send({ user: removedUser.safeForm() })
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async updateOne(req: Request, res: Response, next: NextFunction) {
    try {
      const updateObj = Object.assign({}, req.body, { lastUpdated: new Date() })
      const user = await User.findByIdAndUpdate(req.params.id, updateObj, { new: true })
      if (!user) return res.status(status.NOT_FOUND).send({ error: 'not found' })

      res.status(status.OK).send(user.safeForm())
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }
}
