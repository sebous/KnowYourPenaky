import mongoose from 'mongoose'
import status from 'http-status'
import moment from 'moment'
import { Request, Response, NextFunction } from 'express'
import { flatten, groupBy } from 'lodash'

import { Session, SessionDocument, Person, Drink, DrinkEnum } from '../models/session.model'

export default class SessionController {

  public async create(req: Request, res: Response) {
    try {
      const { name, place, userId } = req.body
      const session = await new Session({
        name: name || `${place} - ${moment().format('DD-MM-YY')}`,
        createdBy: userId,
      }).save()
      res.status(status.CREATED).send(session)
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async getLast(req: Request, res: Response) {
    try {
      const { userId } = req.body
      const session = await Session
        .find({ createdBy: userId, finalized: false })
        .limit(1)
        .sort({ updatedAt: -1 });
      if (session.length === 0) return res.status(status.OK).send(undefined);
      return res.status(status.OK).send(session[0]);
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async addPerson(req: Request, res: Response) {
    try {
      const { sessionId, name } = req.body
      const person: Person = {
        name: name as string,
        drinks: [],
      }
      const session = await Session.findOneAndUpdate(
        { _id: sessionId, finalized: false, 'people.name': { $ne: person.name } },
        {
          $push: { people: person },
        },
        { new: true },
      )
      if (session) return res.status(status.CREATED).send(session)
      res.status(status.NOT_FOUND).send({ error: 'Duplicate person name' })
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  public async addDrink(req: Request, res: Response) {
    try {
      const { sessionId, drinkType, name } = req.body
      const num = Number(drinkType)
      const drink: Drink = { type: (DrinkEnum as any)[num], timeOrdered: moment().toDate() }
      const session = await Session.findOneAndUpdate(
        { _id: sessionId, 'people.name': name, finalized: false },
        {
          $push: { 'people.$.drinks': drink },
        },
        { new: true },
      )
      res.status(status.OK).send(session)
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  summarizeDrinks = async (req: Request, res: Response) => {
    try {
      const { sessionId } = req.body
      const session = await Session.findById(sessionId)
      if (!session) return res.status(status.NOT_FOUND).send({ error: 'Not found' })
      const aggr = this.aggregateDrinks(session)
      res.status(status.OK).send(aggr)
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }

  private aggregateDrinks(session: SessionDocument): any {
    const drinksGrouped = groupBy(flatten(session.people.map(p => p.drinks)), d => d.type)
    const drinks = Object.keys(drinksGrouped).map(key => ({ type: key, count: drinksGrouped[key].length }))
    return drinks
  }

  public async finalize(req: Request, res: Response) {
    try {
      const { sessionId } = req.body
      const session = await Session.findOneAndUpdate({ _id: sessionId, finalized: false }, {
        $set: { datePayed: moment().toDate(), finalized: true },
      }, { new: true })
      if (!session) return res.status(status.NOT_FOUND).send({ error: 'Not found or already finalized' })
      res.status(status.OK).send(session)
    } catch (err) {
      res.status(status.INTERNAL_SERVER_ERROR).send(err)
    }
  }
}

