import express, { Request, Response, NextFunction, ErrorRequestHandler } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import mongoose from 'mongoose'
import passport from 'passport'
import status from 'http-status'
import dotenv from 'dotenv'

import routes from './routes'
import { EventEmitter } from 'events';

const isProduction = process.env.NODE_ENV === 'production'
dotenv.config()
const app = express()

// connect to db
mongoose.connect('mongodb://localhost:27017/KnowYourPenaky', {
  useNewUrlParser: true,
  reconnectTries: 5,
  reconnectInterval: 300,
})
mongoose.connection.on('connected', () => console.log('connected'))
mongoose.connection.on('error', (e: EventEmitter) => console.log(e))

// CORS
app.use((req, res, next) => {
  res.type('application/json')
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, x-access-token, Content-Type, Accept')
  next()
})

// logging
app.use(morgan(isProduction ? 'tiny' : 'dev'))

// middlewares
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(passport.initialize())
// routes
app.use(routes)

type Error = {
  status?: number,
  code?: number,
  message?: string,
};

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err.code === 11000) {
    res.status(status.INTERNAL_SERVER_ERROR).send({ error: 'object already exists' })
  }
  res.status(status.INTERNAL_SERVER_ERROR).send({ error: 500 })
})

// 404
app.get('*', (req: Request, res: Response) => {
  res.status(status.NOT_FOUND).send({ error: 404 })
})

app.listen(7070)

export default app
