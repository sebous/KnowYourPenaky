import { Router } from 'express'
import status from 'http-status'
import apiRoutes from './api'

const router = Router()

router.use('/api', apiRoutes)
router.get('/api', (req, res) => {
  res.status(status.OK).send({ message: '/api, specify route' })
})

export default router
