import { Router } from 'express'
import { requireAuth } from '../../middlewares/passport'
import SessionController from '../../controllers/session.controller'

const ctrl = new SessionController();
const router = Router()

router.post('/create', requireAuth, ctrl.create)
router.post('/addPerson', requireAuth, ctrl.addPerson)
router.post('/addDrink', requireAuth, ctrl.addDrink)
router.post('/summary', requireAuth, ctrl.summarizeDrinks)
router.post('/finalize', requireAuth, ctrl.finalize)
router.post('/last', requireAuth, ctrl.getLast)

export default router
