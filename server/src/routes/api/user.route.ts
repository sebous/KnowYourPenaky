import { Router } from 'express'
import { requireAuth } from '../../middlewares/passport'
import UserController from '../../controllers/user.controller'

const router = Router()
const ctrl = new UserController()

router.post('/register', ctrl.create)
router.post('/login', ctrl.login)
router.post('/fb-login', ctrl.fbLogin)
router.get('/test-token', requireAuth, ctrl.testToken)
router.get('/refresh-token', requireAuth, ctrl.refreshToken)
router.get('/all', requireAuth, ctrl.getAll)
router.get('/:id', requireAuth, ctrl.getOne)

export default router
