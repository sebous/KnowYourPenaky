import { Router } from 'express'
import userRoutes from './user.route'
import sessionRoutes from './session.route'
const router = Router()

router.use('/user', userRoutes)
router.use('/session', sessionRoutes)

export default router
