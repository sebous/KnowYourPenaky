# KnowYourPenaky

Full-stack js app to keep up with our drinking habits

## Client
/client is React.js app with implemented redux store

## Server
/server is express.js REST API written in typescript, with mongoDB as database. JSON web tokens used for authentication, mongoose as ORM.
